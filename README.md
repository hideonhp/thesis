# Graduation Thesis

Ứng dụng nhắn tin tức thời
+ Được thực hiện bởi: Bùi Đức Tiến - 175010157
+ Được hướng dẫn bởi: Thạc Sĩ - Lê Ngọc Hiếu

- Các chức năng hiện tại
+ Đăng nhập, đăng kí tài khoản bằng email.
+ Đăng nhập với tài khoản của google.
+ Tìm kism người dùng bằng tên.
+ Nhắn tin với nhau bằng: hình ảnh tĩnh, văn bản, hình ảnh động.
+ Cá nhân hóa tài khoản bằng cách cho thay đổi: Ảnh đại diện, tên đăng nhập, tên tài khoản, mật khẩu, trạng thái,...
+ SafeArena - tối ưu hóa hiển thị cho các màn hình tai thỏ, camera đục lỗ.
+ Giao diện nhắn tin thân thiện (reponsive).
+ Gửi mail báo cáo lỗi ứng dụng.
+ Giao diện Giới thiệu ứng dụng

- Các chức năng vừa thêm vào
+ Thông tin ứng dụng.

+ Cập nhập giao diện cho nó thân thiện với môi trường phong cách Apple.
+ Thêm vài lỗi nhở ở lúc đăng nhập và cài đặt tài khoản.

- Các chức năng đang phát triển
+ Đa ngôn ngũ :v
+ Fix Bug
+ ...

## Các công nghệ chính

- Sdk Flutter with Dart language For FrontEnd
- Sdk Firebase NonSql Database For BackEnd
- Sdk Firebase Authenticator For Ascess Application
- ...

### Các phần mềm bổ trợ

- Android Studio
- Notepad ++
- Visual Studio Code
- ...

#### Các nguồn tham khảo

- https://flutter.dev/docs for documentation
- https://www.youtube.com/user/GoogleDevelopers for widget
- Indian, Chinese,... From Reddit, StackOverFlow for fixbugs 


