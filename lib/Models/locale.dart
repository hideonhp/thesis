// import 'package:flutter/material.dart';
// import 'package:intl/intl.dart';
// import 'dart:async';
//
// import 'package:thesis/generated/l10n.dart';
//
// import 'package:thesis/generated/intl/messages_all.dart';
// class AppLocalizations {
//   static Future<AppLocalizations> load(Locale locale) {
//     final String name = locale.countryCode.isEmpty
//         ? locale.languageCode
//         : locale.toString();
//     final String localeName = Intl.canonicalizedLocale(name);
//     return initializeMessages(localeName).then((bool _) {
//       Intl.defaultLocale = localeName;
//       return AppLocalizations();
//     });
//   }
//
//   static AppLocalizations of(BuildContext context) {
//     return Localizations.of<AppLocalizations>(context, AppLocalizations);
//   }
// }
//
// class AppLocalizationDelegate extends LocalizationsDelegate<AppLocalizations> {
//   const AppLocalizationDelegate();
//
//   @override
//   bool isSupported(Locale locale) {
//     return ['en', 'vi'].contains(locale.languageCode);
//   }
//
//   @override
//   Future<AppLocalizations> load(Locale locale) {
//     return AppLocalizations.load(locale);
//   }
//
//   @override
//   bool shouldReload(AppLocalizationDelegate old){
//     return false;
//   }
//
// }