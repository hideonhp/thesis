import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:thesis/Models/language.dart';
import 'package:thesis/Pages/HomePage.dart';
import 'package:thesis/Pages/LoginPage.dart';
import 'package:thesis/Pages/SignupPage.dart';
import 'package:thesis/Widgets/AnotherWidget.dart';
import 'package:thesis/Widgets/LoaderWidget.dart';
import 'package:thesis/Widgets/OrAnotherWidget.dart';
import 'package:thesis/Widgets/ProgressDialogWidget.dart';
import 'package:thesis/Widgets/ProgressWidget.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:thesis/Widgets/RoundedButtonWidget.dart';
import 'package:thesis/generated/l10n.dart';

class WelcomeScreen extends StatefulWidget {
  WelcomeScreen({Key key}) : super(key: key);

  @override
  WelcomeScreenState createState() => WelcomeScreenState();
}

class WelcomeScreenState extends State<WelcomeScreen> {
  //Khai báo google
  final GoogleSignIn googleSignIn = GoogleSignIn();
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  SharedPreferences preferences;

  //Biến check
  bool isLoggedIn = false;
  bool isLoading = false;

  //Biến người dùng
  FirebaseUser currentUser;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    //Check đăng nhập hay chưa ngay khi mở app
    isSignedIn();
  }

  //Hàm check đăng nhập hay chưa
  void isSignedIn() async {
    this.setState(() {
      isLoading = true;
    });
    preferences = await SharedPreferences.getInstance();
    isLoggedIn = await googleSignIn.isSignedIn();
    //Nếu đã đăng nhập thì sẽ chuyển qua màn hình đăng nhập
    if (isLoggedIn) {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  HomeScreen(currentUserId: preferences.getString("id"))));
    }
    this.setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    //Kích thước tối đa (cao, rộng) của body
    return WillPopScope(
      //Cảnh báo người dùng thoát ứng dụng
      onWillPop: () => showDialog<bool>(
        context: context,
        builder: (c) => AlertDialog(
          title: Text('Cảnh Báo'),
          content: Text('Bạn Có Chắc Là Sẽ Thoát Ứng Dụng    -_-'),
          actions: [
            FlatButton(
              child: Text('Có'),
              onPressed: () => Navigator.pop(c, true),
            ),
            FlatButton(
              child: Text('Không'),
              onPressed: () => Navigator.pop(c, false),
            ),
          ],
        ),
      ),
      child: Scaffold(
        body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [Colors.lightBlueAccent, Colors.purpleAccent],
            ),
          ),
          alignment: Alignment.center,
          child: SafeArea(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Thesis Graduation",
                  style: TextStyle(
                      fontSize: 62.0,
                      color: Colors.white,
                      fontFamily: "Signatra"),
                ),
                SizedBox(height: size.height * 0.03),
                Image.asset(
                  "assets/images/Chat.png",
                  height: size.height * 0.4,
                ),
                SizedBox(height: size.height * 0.03),
                Padding(
                  padding: EdgeInsets.all(1.0),
                  child: isLoading ? ColorLoader() : Container(),
                  //child: isLoading ? ProgressDialog(message: "Đang đăng nhập, vui lòng đợi...",) : Container(),
                ),
                RoundedButton(
                  text: "Đăng Nhập",
                  press: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) {
                          return LoginScreen();
                        },
                      ),
                    );
                  },
                ),
                RoundedButton(
                  text: "Đăng Ký Tài Khoản",
                  color: Colors.purpleAccent,
                  textColor: Colors.black,
                  press: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) {
                          return SignupScreen();
                        },
                      ),
                    );
                  },
                ),
                OrAnother(),
                GestureDetector(
                  onTap: controlSignIn,
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        Container(
                          width: 270.0,
                          height: 65.0,
                          margin: EdgeInsets.symmetric(horizontal: 10),
                          padding: EdgeInsets.all(20),
                          decoration: BoxDecoration(
                            border: Border.all(
                              width: 2,
                              color: Colors.white,
                            ),
                            shape: BoxShape.circle,
                          ),
                          child: SvgPicture.asset(
                            "assets/icons/Gg.svg",
                            height: 20,
                            width: 20,
                            color: Colors.white,
                          ),
                        ),
                        // SizedBox(height: size.height * 0.01),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  //Hàm đăng nhập bằng google
  Future<Null> controlSignIn() async {
    preferences = await SharedPreferences.getInstance();
    this.setState(() {
      isLoading = true;
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return WillPopScope(
              child: ProgressDialog(
                message: "Đang đăng nhập, Vui lòng đợi...",
              ),
              // onWillPop: () async {
              //   Navigator.pop(context);
              //   return true;
              // },
            );
          });
    });

    GoogleSignInAccount googleUser =
        await googleSignIn.signIn().catchError((onError) {
      Navigator.pop(context);
      Fluttertoast.showToast(
          msg: "Đăng Nhập Thát Bại! - Mã lỗi: acc-1 hoặc" + onError);
    });
    GoogleSignInAuthentication googleAuthentication =
        await googleUser.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
        idToken: googleAuthentication.idToken,
        accessToken: googleAuthentication.accessToken);
    FirebaseUser firebaseUser =
        (await firebaseAuth.signInWithCredential(credential)).user;
    Navigator.pop(context);

    //Đăng nhập thành công
    if (firebaseUser != null) {
      //Kiểm tra đã đăng ký từ trước chưa
      final QuerySnapshot resultQuery = await Firestore.instance
          .collection("users")
          .where("id", isEqualTo: firebaseUser.uid)
          .getDocuments();
      final List<DocumentSnapshot> documentSnapshots = resultQuery.documents;
      //Nếu chưa thì phải lưu user mới vào database
      if (documentSnapshots.length == 0) {
        Firestore.instance
            .collection("users")
            .document(firebaseUser.uid)
            .setData({
          "nickname": firebaseUser.displayName,
          "photoUrl": firebaseUser.photoUrl,
          "id": firebaseUser.uid,
          "aboutMe": "Bạn chưa cập nhập trạng thái...",
          "createdAt": DateTime.now().millisecondsSinceEpoch.toString(),
          "chattingWith": null,
        });

        //Lưu user vào bộ nhớ của user
        currentUser = firebaseUser;
        await preferences.setString("id", currentUser.uid);
        await preferences.setString("nickname", currentUser.displayName);
        await preferences.setString("photoUrl", currentUser.photoUrl);
      } else {
        //Lưu user vào bộ nhớ của user
        currentUser = firebaseUser;
        await preferences.setString("id", documentSnapshots[0]["id"]);
        await preferences.setString(
            "nickname", documentSnapshots[0]["nickname"]);
        await preferences.setString(
            "photoUrl", documentSnapshots[0]["photoUrl"]);
        await preferences.setString("aboutMe", documentSnapshots[0]["aboutMe"]);
      }
      Fluttertoast.showToast(msg: "Đăng Nhập Thành Công!");
      this.setState(() {
        isLoading = false;
      });
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => HomeScreen(
                    currentUserId: firebaseUser.uid,
                  )));
    }
    //Đăng nhập thất bại
    else {
      Navigator.pop(context);
      Fluttertoast.showToast(msg: "Đăng Nhập Thất Bại, Vui Lòng Kiểm Tra Lại!");
      this.setState(() {
        isLoading = false;
      });
    }
  }
}
