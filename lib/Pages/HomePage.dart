import 'dart:async';
import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:thesis/Models/user.dart';
import 'package:thesis/Pages/ChattingPage.dart';
import 'package:thesis/main.dart';
import 'package:thesis/Pages/AccountSettingsPage.dart';
import 'package:thesis/Widgets/ProgressWidget.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:google_sign_in/google_sign_in.dart';

class HomeScreen extends StatefulWidget {
  final String currentUserId;

  HomeScreen({Key key, @required this.currentUserId}) : super(key: key);

  @override
  State createState() => HomeScreenState(currentUserId: currentUserId);
}

class HomeScreenState extends State<HomeScreen> {
  HomeScreenState({Key key, @required this.currentUserId});

  String formattedDate = DateFormat('kk:mm').format(DateTime.now());
  TextEditingController searchTextController = TextEditingController();
  TextEditingController nickNameTextController;
  TextEditingController aboutMeTextController;
  Future<QuerySnapshot> futureSearchResults;
  final FocusNode nickNameFocusNode = FocusNode();
  final FocusNode aboutMeFocusNode = FocusNode();
  final String currentUserId;
  SharedPreferences preferences;
  String id = "";
  String nickname = "";
  String aboutMe = "";
  String photoUrl = "";
  File imageFileAvatar;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    //Khi mở màn hình home thì sẽ load dữ liệu từ local lên
    readDataFromLocal();
  }

  homePageHeader() {
    return AppBar(
      //Xóa nút back (Nhìn cho thẩm mỹ viện :v)
      automaticallyImplyLeading: false,
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.settings,
            size: 30.0,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => Settings()));
          },
        ),
      ],
      backgroundColor: Colors.lightBlue,
      title: Container(
        margin: new EdgeInsets.only(bottom: 4.0),
        child: TextFormField(
          style: TextStyle(fontSize: 18.0, color: Colors.white),
          controller: searchTextController,
          decoration: InputDecoration(
            hintText: "Tìm Kiếm...",
            hintStyle: TextStyle(color: Colors.white),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.grey),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
            ),
            filled: true,
            prefixIcon: Icon(
              Icons.person_pin,
              color: Colors.white,
              size: 30.0,
            ),
            suffixIcon: IconButton(
              icon: Icon(
                Icons.clear,
                color: Colors.white,
              ),
              onPressed: emptyTexFormField,
            ),
          ),
          onFieldSubmitted: controlSearch,
        ),
      ),
    );
  }

  controlSearch(String userName) {
    Future<QuerySnapshot> allFoundUsers = Firestore.instance
        .collection("users")
        .where("nickname", isGreaterThanOrEqualTo: userName)
        .getDocuments();

    setState(() {
      futureSearchResults = allFoundUsers;
    });
  }

  emptyTexFormField() {
    searchTextController.clear();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      //Cảnh báo người dùng thoát ứng dụng
      onWillPop: () => showDialog<bool>(
        context: context,
        builder: (c) => AlertDialog(
          title: Text('Cảnh Báo'),
          content: Text('Bạn Có Chắc Là Sẽ Thoát     -_-'),
          actions: [
            FlatButton(
              child: Text('Có'),
              onPressed: () => Navigator.pop(c, true),
            ),
            FlatButton(
              child: Text('Không'),
              onPressed: () => Navigator.pop(c, false),
            ),
          ],
        ),
      ),
      child: Scaffold(
        appBar: homePageHeader(),
        body: futureSearchResults == null
            ? displayNoSearchResultScreen()
            : displayUserFoundScreen(),
      ),
    );
  }

  //lấy thông tin từ local
  void readDataFromLocal() async {
    preferences = await SharedPreferences.getInstance();
    id = preferences.getString("id");
    nickname = preferences.getString("nickname");
    aboutMe = preferences.getString("aboutMe");
    photoUrl = preferences.getString("photoUrl");
    nickNameTextController = TextEditingController(text: nickname);
    aboutMeTextController = TextEditingController(text: aboutMe);
    setState(() {});
  }

  //Hiển thị người dùng tìm thấy
  displayUserFoundScreen() {
    return SafeArea(
      child: FutureBuilder(
        future: futureSearchResults,
        builder: (context, dataSnapshot) {
          if (!dataSnapshot.hasData) {
            return circularProgress();
          }
          List<UserResult> searchUserResult = [];
          dataSnapshot.data.documents.forEach((document) {
            User eachUser = User.fromDocument(document);
            UserResult userResult = UserResult(eachUser);

            if (currentUserId != document["id"]) {
              searchUserResult.add(userResult);
            }
          });
          return ListView(children: searchUserResult);
        },
      ),
    );
  }

  //Nếu không tìm kiếm thì sẽ ra màn hình này
  displayNoSearchResultScreen() {
    final Orientation orientation = MediaQuery.of(context).orientation;
    return SafeArea(
      child: Container(
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.width * 0.7,
              decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(40.0),
                    bottomLeft: Radius.circular(40.0),
                  )),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 30.0, right: 30.0, top: 70.0),
                    child: Row(
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Tất cả liên hệ",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 26.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.width * 0.02,
                            ),
                            Text(
                              "Bạn có 0 tin nhắn đang chờ",
                              style: TextStyle(
                                color: Colors.blue[50],
                                fontSize: 14.0,
                              ),
                            ),
                          ],
                        ),
                        Spacer(),
                        CircleAvatar(
                          backgroundColor: Colors.white,
                          child: Icon(Icons.add),
                        )
                      ],
                    ),
                  ),
                  //SizedBox(height: MediaQuery.of(context).size.width * 0.01,)
                  SizedBox(
                    height: 70.0,
                  ),
                  Stack(
                    alignment: Alignment.center,
                    overflow: Overflow.visible,
                    children: <Widget>[
                      Container(
                        height: 70.0,
                        width: 300.0,
                        decoration: BoxDecoration(
                            color: Colors.black12,
                            borderRadius: BorderRadius.circular(15.0)),
                      ),
                      Positioned(
                        bottom: 10.0,
                        child: Container(
                          height: 70.0,
                          width: 330.0,
                          decoration: BoxDecoration(
                              color: Colors.white70,
                              borderRadius: BorderRadius.circular(15.0)),
                        ),
                      ),
                      Positioned(
                        bottom: 20.0,
                        child: Container(
                          height: 95.0,
                          width: 350,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(15.0)),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: <Widget>[
                                Container(
                                  decoration: BoxDecoration(
                                    color: Colors.blueGrey,
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(3.0),
                                    child: CachedNetworkImage(
                                      placeholder: (context, url) => Container(
                                        child: CircularProgressIndicator(
                                          strokeWidth: 2.0,
                                          valueColor:
                                          AlwaysStoppedAnimation<Color>(
                                              Colors.lightBlueAccent),
                                        ),
                                        width: 60.0,
                                        height: 60.0,
                                        padding: EdgeInsets.all(20.0),
                                      ),
                                      imageUrl: photoUrl,
                                      width: 60.0,
                                      height: 60.0,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 2,
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Text(
                                              nickname,
                                              style: TextStyle(
                                                color: Colors.black87,
                                                fontSize: 16.0,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            Spacer(),
                                            Text(
                                              formattedDate,
                                              style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 16.0,
                                              ),
                                            ),
                                          ],
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(left: 2.0, top :4.0,),
                                          child: Text(
                                            aboutMe,
                                            style: TextStyle(
                                              color: Colors.black87,
                                              fontSize: 14.0,
                                            ),
                                          ),
                                        ),
                                        //SizedBox(height: 3.0,),
                                        // Row(
                                        //   children: <Widget>[
                                        //     Text("Trạng thái cá nhân chưa được cập nhập !", style: TextStyle(
                                        //       color:  Colors.black87,
                                        //       fontSize: 13.0,
                                        //     ),),
                                        //     //Spacer(),
                                        //     // Icon(
                                        //     //   Icons.star_border,
                                        //     //   color: Colors.orange,
                                        //     //   size: 20.0,
                                        //     // ),
                                        //   ],
                                        // ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            SizedBox(
              height: 30.0,
            ),
            Padding(
              padding: const EdgeInsets.only(
                left: 30.0,
              ),
              child: Row(
                children: <Widget>[
                  Text(
                    "Liên hệ gần đây !",
                    style: TextStyle(
                      color: Colors.black87,
                      fontWeight: FontWeight.bold,
                      fontSize: 16.0,
                    ),
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  Text(
                    "(không có liên hệ gần đây)",
                    style: TextStyle(
                      color: Colors.grey,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 20.0,),





            // InkWell(
            //   onTap: (){},
            //   child: Container(
            //     height: 95.0,
            //     width: 300,
            //     decoration: BoxDecoration(
            //         color: Colors.white,
            //         borderRadius: BorderRadius.circular(15.0)),
            //     child: Padding(
            //       padding: const EdgeInsets.all(12.0),
            //       child: Row(
            //         children: <Widget>[
            //           Container(
            //             decoration: BoxDecoration(
            //               color: Colors.blueGrey,
            //               borderRadius: BorderRadius.circular(10.0),
            //             ),
            //             child: Padding(
            //               padding: const EdgeInsets.all(3.0),
            //               child: Image.asset(
            //                 "assets/images/Tom_Thien_Lanh.bmp",
            //                 height: 60.0,
            //                 width: 60.0,
            //               ),
            //             ),
            //           ),
            //           Expanded(
            //             flex: 2,
            //             child: Padding(
            //               padding: const EdgeInsets.all(10.0),
            //               child: Column(
            //                 children: <Widget>[
            //                   Row(
            //                     children: <Widget>[
            //                       Text(
            //                         "Tom Thiện Lành",
            //                         style: TextStyle(
            //                           color: Colors.black87,
            //                           fontSize: 16.0,
            //                           fontWeight: FontWeight.bold,
            //                         ),
            //                       ),
            //                       Spacer(),
            //                       Text(
            //                         "8:26",
            //                         style: TextStyle(
            //                           color: Colors.grey,
            //                           fontSize: 16.0,
            //                         ),
            //                       ),
            //                     ],
            //                   ),
            //                   Padding(
            //                     padding: const EdgeInsets.all(2.0),
            //                     child: Text(
            //                       "Trạng thái cá nhân chưa được cập nhập !",
            //                       style: TextStyle(
            //                         color: Colors.black87,
            //                         fontSize: 14.0,
            //                       ),
            //                     ),
            //                   ),
            //                   //SizedBox(height: 3.0,),
            //                   // Row(
            //                   //   children: <Widget>[
            //                   //     Text("Trạng thái cá nhân chưa được cập nhập !", style: TextStyle(
            //                   //       color:  Colors.black87,
            //                   //       fontSize: 13.0,
            //                   //     ),),
            //                   //     //Spacer(),
            //                   //     // Icon(
            //                   //     //   Icons.star_border,
            //                   //     //   color: Colors.orange,
            //                   //     //   size: 20.0,
            //                   //     // ),
            //                   //   ],
            //                   // ),
            //                 ],
            //               ),
            //             ),
            //           ),
            //         ],
            //       ),
            //     ),
            //   ),
            // ),
            SizedBox(height: 10.0,),
            // Padding(
            //   padding: const EdgeInsets.only(left: 70.0,right: 70.0,),
            //   child: Container(
            //     width: 70.0,
            //     decoration: BoxDecoration(
            //       color: Colors.red[100],
            //       borderRadius: BorderRadius.circular(5.0),
            //       border: Border.all(color: Colors.blueAccent)
            //     ),
            //     child: Padding(
            //       padding: const EdgeInsets.all(5.0),
            //       // child: Row(
            //       //   children: <Widget>[
            //       //     Container(
            //       //       decoration: BoxDecoration(
            //       //         color: Colors.purple,
            //       //         borderRadius: BorderRadius.circular(5.0),
            //       //       ),
            //       //       child: Padding(
            //       //         padding: const EdgeInsets.all(3.0),
            //       //         child: Text("+++++++++++++++++++++++++++++++++", style: TextStyle(
            //       //           color: Colors.purpleAccent,
            //       //           fontSize: 17.0,
            //       //         ),),
            //       //       ),
            //       //     ),
            //       //   ],
            //       // ),
            //     ),
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}

class UserResult extends StatelessWidget {
  final User eachUser;

  UserResult(this.eachUser);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(4.0),
      child: Container(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            GestureDetector(
              onTap: () => sendUserToChatPage(context),
              child: ListTile(
                leading: CircleAvatar(
                  backgroundColor: Colors.black,
                  backgroundImage:
                      CachedNetworkImageProvider(eachUser.photoUrl),
                ),
                title: Text(
                  eachUser.nickname,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                subtitle: Text(
                  "Ngày Tham Gia: " +
                      DateFormat("dd - MM - yyyy : HH:mm").format(
                          DateTime.fromMillisecondsSinceEpoch(
                              int.parse(eachUser.createdAt))),
                  style: TextStyle(
                    color: Colors.grey,
                    fontSize: 14.0,
                    fontStyle: FontStyle.italic,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  sendUserToChatPage(BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => Chat(
                  receiverId: eachUser.id,
                  receiverAvatar: eachUser.photoUrl,
                  receiverName: eachUser.nickname,
                )));
  }
}
