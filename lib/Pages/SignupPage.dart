import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:thesis/Pages/LoginPage.dart';
import 'package:thesis/Widgets/LoaderWidget.dart';
import 'package:thesis/Widgets/RoundedButtonWidget.dart';
import 'package:thesis/Widgets/RoundedInputFieldWidget.dart';
import 'package:thesis/Widgets/RoundedPasswordFieldWidget.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:thesis/Pages/HomePage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_auth/firebase_auth.dart';

class SignupScreen extends StatefulWidget {
  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  String newNickname;
  String newPassword;
  String newEmail;

  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  SharedPreferences preferences;

  bool isLoggedIn = false;
  bool isLoading = false;
  FirebaseUser currentUser;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isLoading = false;
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    //Kích thước tối đa (cao, rộng) của body
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [Colors.purpleAccent, Colors.lightBlueAccent],
          ),
        ),
        alignment: Alignment.center,
        child: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Welcome New User",
                  style: TextStyle(
                      fontSize: 62.0,
                      color: Colors.white,
                      fontFamily: "Signatra"),
                ),
                SizedBox(height: size.height * 0),
                Image.asset(
                  "assets/images/Signup.png",
                  height: size.height * 0.4,
                ),
                SizedBox(height: size.height * 0),
                RoundedInputField(
                  icon: Icons.email,
                  hintText: "Địa Chỉ Email",
                  onChanged: (value) {
                    newEmail = value;
                  },
                ),
                RoundedInputField(
                  icon: Icons.person_pin,
                  hintText: "Họ và Tên",
                  onChanged: (value) {
                    newNickname = value;
                  },
                ),
                RoundedPasswordField(
                  onChanged: (value) {
                    newPassword = value;
                  },
                ),
                GestureDetector(
                  child: RoundedButton(
                    text: "Đăng Ký",
                    press: () {
                      if (newEmail.contains("@") &&
                          newEmail.contains(".com") &&
                          newEmail.length > 8) {
                        if (newNickname.length > 6) {
                          if (newPassword.length > 8) {
                            controlSignUp();
                          } else {
                            Fluttertoast.showToast(
                                msg: "Mật Khẩu Tối Thiểu 8 Ký tự!!!");
                          }
                        } else {
                          Fluttertoast.showToast(
                              msg: "Tên Tối Thiểu Là 6 Ký Tự!!!");
                        }
                      } else {
                        Fluttertoast.showToast(
                            msg: "Xin Vui Lòng Nhập Đúng Định Dạng Email!!!");
                      }
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(1.0),
                  child: isLoading ? ColorLoader() : Container(),
                ),
                SizedBox(
                  height: size.height * 0.03,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Đã có tài khoản rồi ? ",
                      style: TextStyle(color: Colors.white),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) {
                              return LoginScreen();
                            },
                          ),
                        );
                      },
                      child: Text(
                        "Đăng nhập ngay thôi !",
                        style: TextStyle(
                          color: Colors.white70,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<Null> controlSignUp() async {
    preferences = await SharedPreferences.getInstance();
    this.setState(() {
      isLoading = true;
    });
    //Đăng ký với tài khoản và mật khẩu
    try {
      final FirebaseUser user =
          (await firebaseAuth.createUserWithEmailAndPassword(
        email: newEmail,
        password: newPassword,
      ))
              .user;
    } catch (e) {
      print(e.toString());
    }
//    final FirebaseUser user = (await firebaseAuth.signInWithEmailAndPassword(
//        email: newEmail, password: newPassword)).user;
    FirebaseUser user;
    user = (await firebaseAuth.signInWithEmailAndPassword(
            email: newEmail, password: newPassword))
        .user;
    if (user != null) {
      //Kiểm tra đã đăng ký từ trước chưa
      final QuerySnapshot resultQuery = await Firestore.instance
          .collection("users")
          .where("id", isEqualTo: user.uid)
          .getDocuments();
      final List<DocumentSnapshot> documentSnapshots = resultQuery.documents;
      //Nếu chưa thì phải lưu user mới vào database
      if (documentSnapshots.length == 0) {
        Firestore.instance.collection("users").document(user.uid).setData({
          "nickname": newNickname,
          "photoUrl":
              "gs://thesis-f895e.appspot.com/1uMImt3WVEVDj2AdFcN2f3Trxn83",
          "id": user.uid,
          "aboutMe": "Tiến rất bodoi",
          "createdAt": DateTime.now().millisecondsSinceEpoch.toString(),
          "chattingWith": null,
        });
        //Lưu user vào bộ nhớ của user
        currentUser = user;
        await preferences.setString("id", currentUser.uid);
        await preferences.setString("nickname", newNickname);
        await preferences.setString("photoUrl",
            "gs://thesis-f895e.appspot.com/1uMImt3WVEVDj2AdFcN2f3Trxn83");
      } else {
        //Lưu user vào bộ nhớ của user local
        currentUser = user;
        await preferences.setString("id", documentSnapshots[0]["id"]);
        await preferences.setString(
            "nickname", documentSnapshots[0]["nickname"]);
        await preferences.setString(
            "photoUrl", documentSnapshots[0]["photoUrl"]);
        await preferences.setString("aboutMe", documentSnapshots[0]["aboutMe"]);
      }
      Fluttertoast.showToast(msg: "Đăng Nhập Thành Công!");
      this.setState(() {
        isLoading = false;
      });
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => HomeScreen(
                    currentUserId: user.uid,
                  )));
    }
    //Đăng nhập thất bại
    else {
      Fluttertoast.showToast(msg: "Đăng Nhập Thất Bại, Vui Lòng Kiểm Tra Lại!");
      this.setState(() {
        isLoading = false;
      });
    }
  }
}
