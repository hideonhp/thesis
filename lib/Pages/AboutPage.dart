import 'package:flutter/material.dart';

class About extends StatefulWidget {
  @override
  _AboutState createState() => _AboutState();
}

class _AboutState extends State<About> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        backgroundColor: Colors.lightBlue,
        title: Text(
          "Ai Đồ :v",
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
      ),
      body: SafeArea(
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [Colors.purpleAccent, Colors.lightBlueAccent],
            ),
          ),
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                "This App Was Made By This Guy !",
                style: TextStyle(
                    fontSize: 24.0, color: Colors.white, fontFamily: "Signatra"),
              ),
              SizedBox(height: size.height * 0.03),
              Image.asset(
                "assets/images/Tom_Thien_Lanh.bmp",
                height: size.height * 0.4,
              ),
              SizedBox(height: size.height * 0.03),
              Text(
                "Ke Ke Ke :v",
                style: TextStyle(
                    fontSize: 24.0, color: Colors.white, fontFamily: "Signatra"),
              ),
              
            ],
          ),
        ),
      ),
    );
  }
}
