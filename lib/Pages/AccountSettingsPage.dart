import 'dart:async';
import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:thesis/Models/language.dart';
import 'package:thesis/Pages/AboutPage.dart';
import 'package:thesis/Pages/BugReportPage.dart';
import 'package:thesis/Pages/HomePage.dart';
import 'package:thesis/Widgets/ProgressWidget.dart';
import 'package:thesis/generated/l10n.dart';
import 'package:thesis/main.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class Settings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.bug_report,
              size: 30.0,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => BugReport()));
            },
          ),
        ],
        backgroundColor: Colors.lightBlue,
        title: Text(
          "Cài Đặt Tài Khoản",
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
      ),
      body: SettingsScreen(),
    );
  }
}

class SettingsScreen extends StatefulWidget {
  @override
  State createState() => SettingsScreenState();
}

class SettingsScreenState extends State<SettingsScreen> {
  TextEditingController nickNameTextController;
  TextEditingController aboutMeTextController;
  SharedPreferences preferences;
  String id = "";
  String nickname = "";
  String aboutMe = "";
  String photoUrl = "";
  File imageFileAvatar;
  bool isLoading = false;
  final FocusNode nickNameFocusNode = FocusNode();
  final FocusNode aboutMeFocusNode = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    //Khi mở màn hình cài đặt thì sẽ load dữ liệu từ local lên
    readDataFromLocal();
  }

  //Hàm load từ local
  void readDataFromLocal() async {
    preferences = await SharedPreferences.getInstance();
    id = preferences.getString("id");
    nickname = preferences.getString("nickname");
    aboutMe = preferences.getString("aboutMe");
    photoUrl = preferences.getString("photoUrl");

    nickNameTextController = TextEditingController(text: nickname);
    aboutMeTextController = TextEditingController(text: aboutMe);
    setState(() {});
  }

  //Chọn ảnh và đồng thời cho lên firebase
  Future getImage() async {
    File newImageFile =
        // ignore: deprecated_member_use
        await ImagePicker.pickImage(source: ImageSource.gallery);
    //Ăn rồi cập nhập miết, lười sửa quá :(

    //Nếu chọn ảnh thành công
    if (newImageFile != null) {
      setState(() {
        this.imageFileAvatar = newImageFile;
        isLoading = true;
      });
    }
    //Up lên firebase
    uploadImageToFirebase();
  }

  //Hàm đẩy ảnh lên firebase
  Future uploadImageToFirebase() async {
    String mFileName = id;
    StorageReference storageReference =
        FirebaseStorage.instance.ref().child(mFileName);
    StorageUploadTask storageUploadTask =
        storageReference.putFile(imageFileAvatar);
    StorageTaskSnapshot storageTaskSnapshot;
    storageUploadTask.onComplete.then((value) {
      if (value.error == null) {
        storageTaskSnapshot = value;
        storageTaskSnapshot.ref.getDownloadURL().then((newImageDownloadUrl) {
          photoUrl = newImageDownloadUrl;
          Firestore.instance.collection("users").document(id).updateData({
            "photoUrl": photoUrl,
            "aboutMe": aboutMe,
            "nickname": nickname,
          }).then((data) async {
            await preferences.setString("photoUrl", photoUrl);
            setState(() {
              isLoading = false;
            });
            Fluttertoast.showToast(msg: "Cập Nhập Thành Công !");
          });
        }, onError: (errorMsg) {
          setState(() {
            isLoading = false;
          });
          Fluttertoast.showToast(
              msg: "Đã Xảy Ra Lỗi. Mã Lỗi: img-1 hoặc " + errorMsg);
        });
      }
    }, onError: (errorMsg) {
      setState(() {
        isLoading = false;
      });
      Fluttertoast.showToast(msg: errorMsg.toString());
    });
  }

  //Hàm cập nhập dữ liệu
  void updateData() {
    nickNameFocusNode.unfocus();
    aboutMeFocusNode.unfocus();

    setState(() {
      isLoading = false;
    });
    Firestore.instance.collection("users").document(id).updateData({
      "photoUrl": photoUrl,
      "aboutMe": aboutMe,
      "nickname": nickname,
    }).then((data) async {
      await preferences.setString("photoUrl", photoUrl);
      await preferences.setString("aboutMe", aboutMe);
      await preferences.setString("nickname", nickname);
      setState(() {
        isLoading = false;
      });
      Fluttertoast.showToast(msg: "Cập Nhập Thành Công !");
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) {
            return HomeScreen();
          },
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        SingleChildScrollView(
          child: Column(
            children: <Widget>[
              //Ảnh đại diện
              Container(
                child: Center(
                  child: Stack(
                    children: <Widget>[
                      (imageFileAvatar == null)
                          ? (photoUrl != "")
                              ? Material(
                                  //Hiển thị ảnh cũ
                                  child: CachedNetworkImage(
                                    placeholder: (context, url) => Container(
                                      child: CircularProgressIndicator(
                                        strokeWidth: 2.0,
                                        valueColor:
                                            AlwaysStoppedAnimation<Color>(
                                                Colors.lightBlueAccent),
                                      ),
                                      width: 200.0,
                                      height: 200.0,
                                      padding: EdgeInsets.all(20.0),
                                    ),
                                    imageUrl: photoUrl,
                                    width: 200.0,
                                    height: 200.0,
                                    fit: BoxFit.cover,
                                  ),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(125.0)),
                                  clipBehavior: Clip.hardEdge,
                                )
                              : Icon(
                                  Icons.account_circle,
                                  size: 90.0,
                                  color: Colors.grey,
                                )
                          : Material(
                              //Hiển thị ảnh mới
                              child: Image.file(
                                imageFileAvatar,
                                width: 200.0,
                                height: 200.0,
                                fit: BoxFit.cover,
                              ),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(125.0)),
                              clipBehavior: Clip.hardEdge,
                            ),
                      IconButton(
                        icon: Icon(
                          Icons.camera_alt,
                          size: 100.0,
                          color: Colors.white54.withOpacity(0.3),
                        ),
                        onPressed: getImage,
                        padding: EdgeInsets.all(0.0),
                        splashColor: Colors.transparent,
                        highlightColor: Colors.grey,
                        iconSize: 200.0,
                      ),
                    ],
                  ),
                ),
                width: double.infinity,
                margin: EdgeInsets.all(20.0),
              ),
              //Nhập thông tin ở đây
              Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(1.0),
                    child: isLoading ? circularProgress() : Container(),
                  ),
                  //Tên tài khoản của người dùng
                  Container(
                    child: Text(
                      "Tên Tài Khoản: ",
                      style: TextStyle(
                          fontStyle: FontStyle.italic,
                          fontWeight: FontWeight.bold,
                          color: Colors.lightBlueAccent),
                    ),
                    margin: EdgeInsets.only(left: 10.0, bottom: 5.0, top: 10.0),
                  ),
                  Container(
                    child: Theme(
                      data: Theme.of(context)
                          .copyWith(primaryColor: Colors.lightBlueAccent),
                      child: TextField(
                        decoration: InputDecoration(
                          hintText: "Tên của bạn là ?",
                          contentPadding: EdgeInsets.all(5.0),
                          hintStyle: TextStyle(color: Colors.grey),
                        ),
                        controller: nickNameTextController,
                        onChanged: (value) {
                          nickname = value;
                        },
                        focusNode: nickNameFocusNode,
                      ),
                    ),
                    margin: EdgeInsets.only(left: 30.0, right: 30.0),
                  ),
                  //Tiểu sử của người dùng
                  Container(
                    child: Text(
                      "Tiểu Sử: ",
                      style: TextStyle(
                          fontStyle: FontStyle.italic,
                          fontWeight: FontWeight.bold,
                          color: Colors.lightBlueAccent),
                    ),
                    margin: EdgeInsets.only(left: 10.0, bottom: 5.0, top: 30.0),
                  ),
                  Container(
                    child: Theme(
                      data: Theme.of(context)
                          .copyWith(primaryColor: Colors.lightBlueAccent),
                      child: TextField(
                        decoration: InputDecoration(
                          hintText: "Tiểu sử ?",
                          contentPadding: EdgeInsets.all(5.0),
                          hintStyle: TextStyle(color: Colors.grey),
                        ),
                        controller: aboutMeTextController,
                        onChanged: (value) {
                          aboutMe = value;
                        },
                        focusNode: aboutMeFocusNode,
                      ),
                    ),
                    margin: EdgeInsets.only(left: 30.0, right: 30.0),
                  ),
                ],
                crossAxisAlignment: CrossAxisAlignment.start,
              ),
              //Nút cập nhập
              Container(
                child: FlatButton(
                  onPressed: updateData,
                  child: Text(
                    "Cập Nhập",
                    style: TextStyle(fontSize: 16.0),
                  ),
                  color: Colors.lightBlueAccent,
                  highlightColor: Colors.grey,
                  splashColor: Colors.transparent,
                  textColor: Colors.white,
                  padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                ),
                margin: EdgeInsets.only(top: 50.0, bottom: 1.0),
              ),
              //Nút đăng xuất
              Padding(
                padding: EdgeInsets.only(left: 50.0, right: 50.0),
                child: RaisedButton(
                  color: Colors.red,
                  onPressed: logoutUser,
                  child: Text(
                    "Đăng Xuất",
                    style: TextStyle(color: Colors.white, fontSize: 14.0),
                  ),
                ),
              ),
              //SizedBox(height: MediaQuery.of(context).size.height * 0.2),
              // DropdownButton(
              //   hint: Text("Thay đổi ngôn ngữ"),
              //   onChanged: (Language language) {
              //     _changeLanguage(language);
              //   },
              //   //underline: SizedBox(),
              //   icon: Icon(
              //     Icons.language,
              //     color: Colors.black,
              //   ),
              //   items: Language.languageList()
              //       .map<DropdownMenuItem<Language>>((lang) => DropdownMenuItem(
              //             value: lang,
              //             child: Row(
              //               children: <Widget>[
              //                 Text(lang.flag),
              //                 Text(lang.name)
              //               ],
              //             ),
              //           ))
              //       .toList(),
              // ),
              InkWell(
                onTap: () {
                  openMagicPopup();
                },
                child: Container(
                  height: 80.0,
                  width: 200.0,
                  decoration: BoxDecoration(
                      color: Colors.purple[100],
                      borderRadius: BorderRadius.circular(15.0)),
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.star_border,
                          color: Colors.orange,
                          //size: 20.0,
                        ),
                        Text(
                          "Thông tin ứng dụng",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 14.0,
                          ),
                        ),
                        Icon(
                          Icons.star_border,
                          color: Colors.orange,
                          //size: 20.0,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              // Padding(
              //   padding: EdgeInsets.only(left: 50.0, right: 50.0),
              //   child: RaisedButton(
              //     color: Colors.cyan,
              //     onPressed: () {
              //       Navigator.push(
              //         context,
              //         MaterialPageRoute(
              //           builder: (context) {
              //             return About();
              //           },
              //         ),
              //       );
              //     },
              //     child: Text(
              //       "Thông Tin Ứng Dụng",
              //       style: TextStyle(color: Colors.white, fontSize: 14.0),
              //     ),
              //   ),
              // ),
            ],
          ),
          padding: EdgeInsets.only(left: 15.0, right: 15.0),
        ),
      ],
    );
  }

  void _changeLanguage(Language language) {
    print(language.languageCode);
    print(S.delegate);
    Intl.defaultLocale = language.languageCode;
  }

  final GoogleSignIn googleSignIn = GoogleSignIn();

  //Hàm đăng xuất
  Future<Null> logoutUser() async {
    await FirebaseAuth.instance.signOut();
    googleSignIn.disconnect();
    googleSignIn.signOut();

    this.setState(() {
      isLoading = false;
    });
    //Đăng xuất xong sẽ đẩy ra màn hình đăng nhập đồng thời ko cho quay lại
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => MyApp()),
        (Route<dynamic> route) => false);
  }

  void openMagicPopup() {
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        builder: (BuildContext bc) {
          return Padding(
            padding: const EdgeInsets.all(10.0),
            child: Container(
              height: 500.0,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(30.0),
              ),
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                              color: Colors.blueGrey,
                              borderRadius: BorderRadius.circular(20.0)),
                          child: Padding(
                            padding: const EdgeInsets.all(3.0),
                            child: Image.asset(
                              "assets/images/Tom_Thien_Lanh.bmp",
                              height: 60.0,
                              width: 60.0,
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Text(
                                      "Bùi Đức Tiến",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16.0),
                                    ),
                                    Spacer(),
                                    Icon(
                                      Icons.local_library,
                                      color: Colors.blue[700],
                                    ),
                                    Icon(
                                      Icons.local_hospital,
                                      color: Colors.blue[700],
                                    ),
                                  ],
                                ),
                                Text(
                                  "Mã SSv: 1751010157",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 14.0),
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),

                    SizedBox(height: 20.0,),

                    Text("Đề tài: Tìm hiểu và ứng dụng Flutter,", style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 18.0
                    ),),
                    SizedBox(height: 10.0,),
                    Text("Để có thể hoàn thiện (đồ án/luận văn) tốt nghiệp này, cũng như các kết quả nghiên cứu của (đồ án/luận văn)  này, em xin trân trọng cảm ơn thầy TS. Lê Ngọc Hiếu (Giảng viên hướng dẫn chính thức). Thầy đã tận tình và giúp đỡ cho em trong suốt quá trình nghiên cứu và thực hiện (đồ án/luận văn) này.", style: TextStyle(
                        color: Colors.black,
                        fontSize: 18.0
                    ),),
                    SizedBox(height: 15.0,),
                    Text("Giảng viên hướng dẫn", style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 18.0
                    ),),
                    SizedBox(height: 5.0,),
                    Text("Lê Ngọc Hiếu", style: TextStyle(
                        color: Colors.black,
                        fontSize: 18.0
                    ),),
                    SizedBox(height: 15.0,),

                    Container(
                      decoration: BoxDecoration(
                          color: Colors.blue[50],
                          borderRadius: BorderRadius.circular(30.0)
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Row(
                          children: <Widget>[
                            Text("Gửi tin nhăn riêng cho Tiến ?", style: TextStyle(
                                color: Colors.blueGrey,
                                fontSize: 18.0
                            ),),
                            Spacer(),
                            CircleAvatar(
                              backgroundColor: Colors.blue[600],
                              child: Icon(Icons.arrow_forward, color: Colors.white,),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }
}
