import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:thesis/Pages/SignupPage.dart';
import 'package:thesis/Widgets/LoaderWidget.dart';
import 'package:thesis/Widgets/NewAccountCheckWidget.dart';
import 'package:thesis/Widgets/RoundedButtonWidget.dart';
import 'package:thesis/Widgets/RoundedInputFieldWidget.dart';
import 'package:thesis/Widgets/RoundedPasswordFieldWidget.dart';

import 'HomePage.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  String userEmail;
  String userPassword;
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  SharedPreferences preferences;
  bool isLoggedIn = false;
  bool isLoading = false;
  FirebaseUser currentUser;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isLoading = false;
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    //Kích thước tối đa (cao, rộng) của body
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [Colors.lightBlueAccent, Colors.purpleAccent],
          ),
        ),
        alignment: Alignment.center,
        child: SafeArea(
          child: SingleChildScrollView(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "The Thesis Graduation",
                    style: TextStyle(
                        fontSize: 62.0,
                        color: Colors.white,
                        fontFamily: "Signatra"),
                  ),
                  SizedBox(height: size.height * 0.03),
                  Image.asset(
                    "assets/images/Login.png",
                    height: size.height * 0.4,
                  ),
                  SizedBox(height: size.height * 0.03),
                  RoundedInputField(
                    icon: Icons.email,
                    hintText: "Địa Chỉ Email",
                    onChanged: (value) {
                      userEmail = value;
                    },
                  ),
                  RoundedPasswordField(
                    onChanged: (value) {
                      userPassword = value;
                    },
                  ),
                  GestureDetector(
                    child: RoundedButton(
                      text: "Đăng Nhập",
                      press: () {
                        controlLogIn();
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(1.0),
                    child: isLoading ? ColorLoader() : Container(),
                  ),
                  SizedBox(
                    height: size.height * 0.03,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "Chưa có tài khoản à ?",
                        style: TextStyle(color: Colors.white),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                              builder: (context) {
                                return SignupScreen();
                              },
                            ),
                          );
                        },
                        child: Text(
                          "Làm 1 cái mới nào !",
                          style: TextStyle(
                            color: Colors.white70,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                ]),
          ),
        ),
      ),
    );
  }


  Future<Null> controlLogIn() async {
    preferences = await SharedPreferences.getInstance();
    this.setState(() {
      isLoading = true;
    });
    FirebaseUser user;
    FirebaseAuth Auth;
    user = (await firebaseAuth.signInWithEmailAndPassword(
        email: userEmail, password: userPassword))
        .user;
    if(user.uid == null){
      Fluttertoast.showToast(msg: "Sai Tài Khoản Hoặc Mật Khẩu!!!");
      this.setState(() {
        isLoading = false;
      });
    }else{
      final QuerySnapshot resultQuery = await Firestore.instance
          .collection("users")
          .where("id", isEqualTo: user.uid)
          .getDocuments();
      final List<DocumentSnapshot> documentSnapshots = resultQuery.documents;
      currentUser = user;

      await preferences.setString("id", documentSnapshots[0]["id"]);
      await preferences.setString(
          "nickname", documentSnapshots[0]["nickname"]);
      await preferences.setString(
          "photoUrl", documentSnapshots[0]["photoUrl"]);
      await preferences.setString("aboutMe", documentSnapshots[0]["aboutMe"]);
      Fluttertoast.showToast(msg: "Đăng Nhập Thành Công!");
      this.setState(() {
        isLoading = false;
      });
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => HomeScreen(
                currentUserId: user.uid,
              )));
    }

  }
}
