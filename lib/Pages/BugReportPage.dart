import 'package:flutter/material.dart';
import 'package:mailer/mailer.dart';
import 'package:mailer/smtp_server.dart';
import 'package:thesis/Pages/HomePage.dart';
import 'package:thesis/Widgets/CustomDialog.dart';
import 'package:thesis/Widgets/RoundedInputFieldWidget.dart';

class BugReport extends StatefulWidget {
  @override
  _BugReportState createState() => _BugReportState();
}

class _BugReportState extends State<BugReport> {
  String userName;
  String userMessage;
  String userTitle;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        backgroundColor: Colors.lightBlue,
        title: Text(
          "Báo lỗi ứng dụng",
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
      ),
      body: SafeArea(
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [Colors.purpleAccent, Colors.lightBlueAccent],
            ),
          ),
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: size.height * 0.03),
              Image.asset(
                "assets/images/Tom_Thien_Lanh.bmp",
                height: size.height * 0.4,
              ),
              SizedBox(height: size.height * 0.03),
              RoundedInputField(
                icon: Icons.add_box,
                hintText: "Chức Năng Muốn Phản Hồi ?",
                onChanged: (value) {
                  userTitle = value;
                },
              ),
              RoundedInputField(
                icon: Icons.message,
                hintText: "Nội Dung Muốn Phản Hồi ?",
                onChanged: (value) {
                  userMessage = value;
                },
              ),
              SizedBox(height: size.height * 0.08),
              // Container(
              //   margin: EdgeInsets.symmetric(vertical: 10),
              //   padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
              //   width: size.width * 0.8,
              //   //height: size.width *1,
              //   decoration: BoxDecoration(
              //     color: Colors.white,
              //     borderRadius: BorderRadius.circular(29)
              //   ),
              //   child: TextFormField(
              //     decoration: InputDecoration(
              //         icon: Icon(
              //       Icons.message,
              //       color: Colors.green,
              //     )),
              //   ),
              // ),

              MaterialButton(
                minWidth: size.width * 0.3,
                height: size.width * 0.3 * 0.6,
                //height: size.width * 0.09,
                color: Colors.purple,
                child: Text(
                  "Gửi Phản Hồi",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  sendMail(userName, userTitle, userMessage);
                  showDialog(context: context, builder: (context) => CustomDialog(
                    title: "Cảm ơn bạn rất nhiều",
                    description: "Phản hồi của bạn đã được gửi thành công. Những yêu cầu của bạn sẽ được cập nhập trong tương lại không xa !",
                  ));
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  sendMail(userName, userTitle, userMessage) async {
    String username = '1751010157tien@ou.edu.vn';
    String password = '12345678qQ';
    String title =
        'Lỗi "' + userTitle + '" tại thời điểm ' + DateTime.now().toString();

    final smtpServer = gmail(username, password);
    // Sử dụng SmtpServer class để config SMTP server:
    // final smtpServer = SmtpServer('smtp.domain.com');

    // Tạo thông báo.
    final message = Message()
      ..from = Address(username, 'Báo cáo lỗi và phản hồi')
      // ..from = Address(username, userName)
      ..recipients.add('hideonhp@gmail.com')
      ..ccRecipients.addAll(['hideonhp@gmail.com', 'hideonhp@gmail.com'])
      ..bccRecipients.add(Address('hideonhp@gmail.com'))
      ..subject = title
      ..text = 'This is the plain text.\nThis is line 2 of the text part.'
      ..html = userMessage;

    try {
      final sendReport = await send(message, smtpServer);
      print('Message sent: ' + sendReport.toString());
    } on MailerException catch (e) {
      print('Message not sent.');
      for (var p in e.problems) {
        print('Problem: ${p.code}: ${p.msg}');
      }
    }
    // final equivalentMessage = Message()
    //   ..from = Address(username, name)
    //   ..recipients.add('hideonhp@gmail.com')
    //   ..ccRecipients.addAll(['hideonhp@gmail.com', 'hideonhp@gmail.com'])
    //   ..bccRecipients.add(Address('hideonhp@gmail.com'))
    //   ..subject = ( userTitle + {DateTime.now()})
    //   ..text = 'This is the plain text.\nThis is line 2 of the text part.'
    //   ..html = userMessage;

    // final sendReport2 = await send(equivalentMessage, smtpServer);
    //
    // // Sending multiple messages with the same connection
    //
    // Create a smtp client that will persist the connection
    var connection = PersistentConnection(smtpServer);

    // Send the first message
    await connection.send(message);

    // // send the equivalent message
    // await connection.send(equivalentMessage);

    // close the connection
    await connection.close();



    // return AlertDialog(
    //   title: Text("dasd"),
    //   content: Text("asdasjhgdjhsfjkhasdjkfh"),
    //   actions: <Widget>[
    //     FlatButton(
    //       child: Text("aa"),
    //       onPressed: (){},
    //     ),
    //     FlatButton(
    //       child: Text("bb"),
    //       onPressed: (){},
    //     ),
    //   ],
    // );
    // return Dialog(
    //   shape: RoundedRectangleBorder(
    //     borderRadius: BorderRadius.circular(20.0),
    //   ),
    //   child: Container(
    //     height: 200,
    //     child: Padding(
    //       padding: EdgeInsets.all(10.0),
    //       child: Column(
    //         crossAxisAlignment: CrossAxisAlignment.center,
    //         mainAxisAlignment: MainAxisAlignment.center,
    //         children: <Widget>[
    //           Padding(
    //             padding: const EdgeInsets.only(left: 8.0),
    //             child: TextField(
    //               decoration: InputDecoration(
    //                 border: InputBorder.none,
    //                 hintText: "Cảm ơn bạn đã giúp mình cải thiện sản phẩm này :D"
    //               ),
    //             ),
    //           ),
    //           SizedBox(height: 20.0),
    //           SizedBox(
    //             width: 320.0,
    //             child: RaisedButton(
    //               color: Color(0xFF1BC0C5),
    //               onPressed: (){},
    //               child: Text("Trở về trang chủ", style: TextStyle(
    //                 color: Colors.white,
    //               ),),
    //             ),
    //           )
    //         ],
    //       ),
    //     ),
    //   ),
    // );
  }
}
