import 'package:flutter/material.dart';
import 'package:thesis/Pages/WelcomePage.dart';

import 'generated/l10n.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Thesis',
      theme: ThemeData(
        primaryColor: Colors.lightBlueAccent,
      ),
      // onGenerateRoute: CustomRoute.allRoutes,
      // initialRoute: homeRoute,
      supportedLocales:
        S.delegate.supportedLocales,
        // Locale('vi', 'VN'),
        // Locale('en', 'US'),
      home: WelcomeScreen(),
      debugShowCheckedModeBanner: false,
    );
  }
}
