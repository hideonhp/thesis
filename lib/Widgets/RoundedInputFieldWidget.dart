import 'package:flutter/material.dart';
import 'package:thesis/Widgets/TextFiledContainerWidget.dart';

class RoundedInputField extends StatelessWidget {
  final String hintText;
  final IconData icon;
  final ValueChanged<String> onChanged;
  final controller;
  final String error;

  const RoundedInputField({
    Key key,
    this.controller,
    this.hintText,
    this.icon,
    this.onChanged,
    this.error,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextFormField(
        controller: controller,
        onChanged: onChanged,
        decoration: InputDecoration(
          icon: Icon(
            icon,
            color: Colors.green,
          ),
          hintText: hintText,
          border: InputBorder.none,
        ),
      ),
    );
  }
}
