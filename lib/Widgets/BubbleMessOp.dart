// Define a CustomPainter to paint the bubble background.
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class BubblePainterOp extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final Paint paint = Paint()
      ..color = Colors.deepPurpleAccent
      ..style = PaintingStyle.fill;
    final Path bubble = Path()
      ..moveTo(5, size.height - 5)
      ..quadraticBezierTo(-5, size.height, -16, size.height - 4)
      ..quadraticBezierTo(-5, size.height - 5, 0, size.height - 17)
      ..cubicTo(
          7.61, size.height, 0.0, size.height - 7.61, 0.0, size.height - 17.0)
      ..lineTo(0.0, 17.0)
      ..cubicTo(0.0, 7.61, 7.61, 0.0, 17.0, 0.0)
      ..lineTo(size.width - 21, 0.0)
      ..cubicTo(size.width - 11.61, 0.0, size.width - 4.0, 7.61,
          size.width - 4.0, 17.0)
      ..lineTo(size.width - 4.0, size.height - 11.0)
      ..cubicTo(size.width, size.height, size.width, size.height - 20.0,
          size.width - 20.0, size.height - 5.0)
      //..lineTo(size.width + 0.05, size.height - 0.01)
      // ..cubicTo(size.width - 4.07, size.height + 0, size.width - 8.16,
      //     size.height - 1.06, size.width - 11.04, size.height - 4.04)
      // ..cubicTo(size.width - 0.0, size.height, size.width - 0.0, size.height,
      //     size.width - 0.0, size.height)
      ..close();
    canvas.drawPath(bubble, paint);
  }

  @override
  bool shouldRepaint(BubblePainterOp oldPainter) => false;
}

// This is my custom RenderObject.
class BubbleMessageOp extends SingleChildRenderObjectWidget {
  BubbleMessageOp({
    Key key,
    this.painter,
    Widget child,
  }) : super(key: key, child: child);

  final CustomPainter painter;

  @override
  RenderCustomPaint createRenderObject(BuildContext context) {
    return RenderCustomPaint(
      painter: painter,
    );
  }

  @override
  void updateRenderObject(
      BuildContext context, RenderCustomPaint renderObject) {
    renderObject..painter = painter;
  }
}