import 'package:flutter/material.dart';

class NewAccountCheck extends StatelessWidget {
  final bool login;
  final Function press;

  const NewAccountCheck({
    Key key,
    this.login = true,
    this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          login ? "Chưa có tài khoản à ?  ":"Đã có tài khoản rồi ư ?  ",
          style: TextStyle(color: Colors.white),
        ),
        GestureDetector(
          onTap: press,
          child: Text(
            login ? "Làm 1 cái mới nào ! ":"Đăng nhập ngay thôi !",
            style: TextStyle(
              color: Colors.white70,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ],
    );
  }
}
